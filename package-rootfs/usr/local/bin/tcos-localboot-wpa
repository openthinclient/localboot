#!/usr/bin/python3

import os
import signal
import sys
import time
import tcos.logger
from tcos.utils import run

from pathlib import Path

LOG = tcos.logger.get()

WPA_CONF_DIR = Path('/etc/wpa_supplicant/')
WPA_CONF_PATH = Path('/tcos/link/localboot/wpa_supplicant.conf')


def wait_for(ifname):
    cancel_after_time = time.time() + 3
    sys_entry = os.path.join('/sys/class/net', ifname)
    while time.time() < cancel_after_time:
        if os.path.exists(sys_entry):
            break
        time.sleep(.1)
    else:
        LOG.error('Interface "%s" not available.', ifname)
        raise SystemExit(2)


def run_if_up(ifname):
    wait_for(ifname)

    # Clean up if systemd stops us
    signal.signal(signal.SIGTERM, lambda signum, frame: run_if_down(ifname))

    # Link global wpa_supplicant.conf to interface-specific file and start
    # wpa_supplicant. Use restart in case it's already running.
    wpa_if_conf_path = WPA_CONF_DIR / f'wpa_supplicant-{ifname}.conf'
    if not (wpa_if_conf_path.is_symlink() or wpa_if_conf_path.exists()):
        wpa_if_conf_path.symlink_to(WPA_CONF_PATH)
    run('/usr/bin/systemctl', 'restart', f'wpa_supplicant@{ifname}')

    # Wait for control interface to appear; bail after 10 seconds
    socket_path = Path(f'/var/run/wpa_supplicant/{ifname}')
    for i in range(100):
        if socket_path.exists():
            break
        time.sleep(.1)
    else:
        LOG.error('Failed to start wpa_supplicant for %s', ifname)
        return

    # Restart tcos-ws so it sends the new MAC address to the server
    run('/usr/bin/systemctl', 'restart', 'tcos-ws.service')

    # Wait for connection and run DHCP
    run('/usr/sbin/wpa_cli',
        '-i', ifname,
        '-a', sys.argv[0],)


def run_if_down(ifname):
    wpa_if_conf_path = WPA_CONF_DIR / f'wpa_supplicant-{ifname}.conf'
    if wpa_if_conf_path.is_symlink():
        wpa_if_conf_path.unlink()


def on_wpa_cli_connected(ifname):
    run('/usr/bin/systemctl', 'restart', f'tcos-dhcp-client@{ifname}.service')


if __name__ == '__main__':
    if not len(sys.argv) == 3:
        LOG.error('Usage: %s <interface> up|down', sys.argv[0])
        sys.exit(1)

    ifname, action = sys.argv[1:]

    # called from udev
    if action == 'up':
        run_if_up(ifname)
    elif action == 'down':
        run_if_down(ifname)
    # called from wpa_cli
    elif action == 'CONNECTED':
        on_wpa_cli_connected(ifname)
    elif action == 'DISCONNECTED':
        pass
    # called from misbehaving user
    else:
        LOG.error('Unknown argument: %s', action)
        sys.exit(1)
