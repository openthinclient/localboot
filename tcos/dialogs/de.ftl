localboot_title = localboot

select_drive_title = Laufwerk auswählen

ask_install_or_update = Der Datenträger scheint bereits mit localboot geflasht
    zu sein. Möchten Sie nur den Inhalt aktualisieren (schneller) oder möchten
    Sie neu initialisieren?
ask_install_or_update_title = Datenträger neu initialisieren?
ask_install_or_update_confirm = Neu initialisieren
ask_install_or_update_cancel = Nur Inhalt aktualisieren

ask_wipe_disk = Bitte bestätigen Sie die Löschung von {$device}.
ask_wipe_disk_title = Datenträger löschen?
ask_wipe_disk_confirm = Ja, Datenträger löschen
ask_wipe_disk_cancel = Installation abbrechen

installation_progress_title = Installiere localboot
installation_progress_umount = Warte auf Verfügbarkeit des Geräts.
installation_progress_device_init = 1 / 4 – Initialisiere Gerät. Erstelle Partitionen und Dateisysteme.
installation_progress_bootloader = 2 / 4 – Installiere Bootloader und dessen Konfiguration.
installation_progress_configuration = 3 / 4 – Schreibe Konfigurationsdaten.
installation_progress_files = 4 / 4 – Kopiere Dateien von OS, Anwendungen und dem Custom Folder.

    {$progress}%
installation_progress_syncing = Synchronisiere gecachte Daten mit dem Datenträger.

installation_aborted_title = Die localboot Installation wurde abgebrochen.
installation_aborted = Die Installation wurde abgebrochen.

    localboot wird nicht korrekt funktionieren.

    Wenn Sie es erneut versuchen möchten, starten Sie bitte die Installation neu.

installation_finished_title = localboot Installation abgeschlossen.
installation_finished = Die Installation war erfolgreich.

    Sie können das System jetzt neu starten und die Bootreihenfolge im BIOS ändern, um vom lokalen Datenträger zu booten.

update_finished_title = Ihr ThinClient wurde aktualisiert.
update_finished = Bitte starten Sie so bald wie möglich neu.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = localboot Fehler

error-updater-title = localboot Updater Fehler

error_no_config_for_device = Keine localboot-Konfiguration für dieses Gerät gefunden.

error_localboot_active = Der Client ist bereits geflasht.

error_no_devices_found = Keine Festplatten gefunden.

error_device_not_found = {$device} nicht gefunden oder kein gültiges Laufwerk.

error_device_too_small = {$device} hat nicht genügend Speicherkapazität
    ({$min_size} erforderlich).

error_partitioning_failed = Partitionierung fehlgeschlagen.

error_filesystem_creation_failed = Dateisystemerstellung fehlgeschlagen.

error_mount_failed = Das Einhängen der Partitionen ist fehlgeschlagen.

error_device_init_failed = Initialisierung des Geräts fehlgeschlagen.

error_bootloader_install_failed = Bootloader-Installation fehlgeschlagen.

error_files_install_failed = Kopieren der Dateien fehlgeschlagen.

error_uncaught = Ein unerwarteter Fehler ist aufgetreten.

    {$error}

    Bitte informieren Sie Ihren Administrator.
