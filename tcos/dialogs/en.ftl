localboot_title = localboot

select_drive_title = Select drive

ask_install_or_update = The drive seems already flashed with localboot. Do you
    like to update the content only (faster) or do you want to reinitialize?
ask_install_or_update_title = Reinitialize disk?
ask_install_or_update_confirm = Reinitialize
ask_install_or_update_cancel = Update content only

ask_wipe_disk = Please confirm deletion of {$device}.
ask_wipe_disk_title = Wipe disk?
ask_wipe_disk_confirm = Yes, wipe disk
ask_wipe_disk_cancel = Cancel installation

installation_progress_title = Installing localboot
installation_progress_umount = Waiting for device availability.
installation_progress_device_init = 1 / 4 – Initializing device. Creating partitions and file systems.
installation_progress_bootloader = 2 / 4 – Installing bootloader and its configuration.
installation_progress_configuration = 3 / 4 – Writing configuration data.
installation_progress_files = 4 / 4 – Copying files from OS, applications and the Custom Folder.

        {$progress}%
installation_progress_syncing = Synchronizing cached data to the drive.

installation_aborted_title = localboot installation was aborted.
installation_aborted = The installation was aborted.

    localboot will not work correctly.

    If you want to try again, please restart the installation.

installation_finished_title = localboot installation finished.
installation_finished = The installation was successful.

    You can now restart the system and change the boot order in the BIOS to boot from the local disk.

update_finished_title = Your thin client has been updated.
update_finished = Please restart as soon as possible.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = localboot error

error-updater-title = localboot updater error

error_no_config_for_device = No localboot configuration found for this device.

error_localboot_active = Client is already flashed.

error_no_devices_found = No disks found.

error_device_not_found = {$device} not found or not a valid disk.

error_device_too_small = {$device} has insufficient storage capacity
    ({$min_size} required).

error_partitioning_failed = Failed to create partitions.

error_filesystem_creation_failed = Failed to create filesystems.

error_mount_failed = Failed to mount partitions.

error_device_init_failed = Failed to initialize the device.

error_bootloader_install_failed = Failed to install bootloader.

error_files_install_failed = Failed to copy files.

error_uncaught = An unexpected error occurred.

    {$error}

    Please contact your administrator.
