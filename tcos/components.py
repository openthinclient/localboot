#!/usr/bin/python3


from functools import cached_property
import json
import os
import psutil
from pathlib import Path
import re
import shlex
import shutil
import subprocess
import sys
from inspect import cleandoc
from types import MappingProxyType

from tcos import logger
import tcos.system
from tcos.utils import run


SIZE_BOOT="+128M"
SIZE_SYSTEM="+6144M"

WORK_DIR = Path('/run/tcos/localboot')

DEBUG = (tcos.system.log_level == 'DEBUG')


class classproperty:
    def __init__(self, func):
        self.fget = func
    def __get__(self, instance, owner):
        return self.fget(owner)


# helper for rsync --dry-run --info=del,progress2
def _rsync_will_transfer(rsync_output):
    if '\ndeleting ' in '\n'+rsync_output:
        return True
    transfer_info = rsync_output.strip().rsplit('\n')[-1]
    if transfer_info.split()[0] != '0':
        return True
    return False


class LocalbootError(Exception):
    pass

class Device:
    def __init__(self, dev_path):
        self.dev_path = dev_path

    _lsblk_info = {}

    @classmethod
    def _update_lsblk_info(cls):
        lsblk = run(
            '/usr/bin/lsblk', '--json',
            '--output=type,name,size,vendor,model,uuid,partlabel',
            '--paths', '--bytes',
            stdout=True, stderr=True
        )
        if lsblk.returncode != 0:
            return False

        try:
            devices = json.loads(lsblk.stdout)
        except json.JSONDecodeError:
            logger.exception('Failed to parse lsblk output %s', lsblk.stdout)
            return False

        cls._lsblk_info = MappingProxyType({
            dev['name']: dev
            for dev in devices['blockdevices']
            if dev['type'] == 'disk'
        })

        return True

    @classmethod
    def invalidate_lsblk_info(cls):
        cls._lsblk_info = {}

    @classmethod
    def get_all(cls):
        if not cls._lsblk_info:
            cls._update_lsblk_info()
        return cls._lsblk_info

    @property
    def info(self):
        return self.get_all().get(self.dev_path)

    @property
    def size(self):
        return self.info.get('size', 0)

    @property
    def has_localboot_installed(self):
        labels = [p.get('partlabel') for p in self.info.get('children', [])]
        return (labels
                and all(l and l.startswith('LOCALBOOT_') for l in labels))

    @property
    def boot_part_info(self):
        parts = self.info.get('children', [])
        if len(parts) < 1:
            logger.error('Device %s has no partitions', self.dev_path)
            return None
        return parts[0]

    @property
    def system_part_info(self):
        parts = self.info.get('children', [])
        if len(parts) < 2:
            logger.error('Device %s has no system partition', self.dev_path)
            return None
        return parts[1]

    @property
    def homes_part_info(self):
        parts = self.info.get('children', [])
        if len(parts) < 3:
            logger.error('Device %s has no homes partition', self.dev_path)
            return None
        return parts[2]

    @classproperty
    def localboot_device(cls):
        lb_uuid = tcos.system.boot_arguments.get('LB_UUID')
        for path, info in cls.get_all().items():
            for part in info.get('children', ()):
                if part['partlabel'] != 'LOCALBOOT_SYSTEM':
                    continue

                if tcos.system.is_localboot and not tcos.system.is_installer:
                    if part['uuid'] == lb_uuid:
                        return Device(path)
                    continue

                if tcos.system.is_installer and lb_uuid == part['uuid']:
                    continue # don't return the install stick itself

                return Device(path)

    @classproperty
    def installer_device(cls):
        if not tcos.system.is_installer:
            return None

        lb_uuid = tcos.system.boot_arguments.get('LB_UUID')
        for path, info in cls.get_all().items():
            for part in info.get('children', ()):
                if (part['partlabel'] == 'LOCALBOOT_SYSTEM'
                        and part['uuid'] == lb_uuid):
                    return Device(path)


    def create_partitions(self, cancelled, hybrid_mbr=False):

        logger.debug('Wiping existing partitions and filesystems')
        wipefs = run('/usr/sbin/wipefs', '--all', self.dev_path,
                     check_cancel=cancelled, stderr=True)
        if cancelled():
            return True
        if wipefs.returncode != 0:
            return False

        logger.debug('Creating GPT partitions')
        sgdisk = run(
            '/usr/sbin/sgdisk', self.dev_path,
            # Partition 1: EFI Boot partition
            f'--new=1:2048:{SIZE_BOOT}',
            '--change-name=1:LOCALBOOT_B',
            '--typecode=1:ef00',
            '--attributes=1:set:2', # legacy BIOS bootable
            # Partition 2: System partition
            f'--new=2:0:{SIZE_SYSTEM}',
            '--change-name=2:LOCALBOOT_SYSTEM',
            '--typecode=2:8300',
            # Partition 3: Homes partition
            '--new=3:0:0',
            '--change-name=3:LOCALBOOT_HOMES',
            '--typecode=3:8300',
            check_cancel=cancelled,
            stderr=True
        )
        if cancelled():
            return True
        if sgdisk.returncode != 0:
            return False

        if hybrid_mbr:
            logger.debug('Creating hybrid MBR')
            gdisk = run(
                '/usr/sbin/gdisk', self.dev_path,
                input='\n'.join((
                    'r', # Enter recovery and transformation options
                    'h', # Make hybrid MBR
                    '1', # Add GPT partition 1 to MBR
                    'n', # Cancel: place EFI GPT (0xEE) partition first in MBR
                    '',  # Confirm: use default MBR partition code
                    'y', # Set bootable flag
                    'n', # Cancel: edit other partitions
                    'w', # Write table to disk and exit
                    'y', # Confirm write and exit
                    '',  # final Return for above confirmation
                )),
                check_cancel=cancelled,
                stdout=True, stderr=True,
            )
            logger.debug(gdisk.stdout)
            if cancelled():
                return True
            if gdisk.returncode != 0:
                return False

        logger.debug('Writing MBR boot code')
        dd = run(
            '/usr/bin/dd',
            'bs=440', 'count=1', 'conv=notrunc',
            'if=/opt/localboot/tcos/bin/gptmbr.bin',
            f'of={self.dev_path}',
            check_cancel=cancelled,
            stderr=True,
        )
        if cancelled():
            return True
        if dd.returncode != 0:
            return False

        # Wait for udev so the subsequent lsblk will have the correct info
        run('/usr/bin/udevadm', 'settle')
        self.invalidate_lsblk_info()

        return True


    def create_filesystems(self, cancelled):
        logger.debug('Creating filesystems')

        mkfs = run(
            '/usr/sbin/mkfs.vfat',
            '-F', '32',  # FAT size 32 bit
            '-n', 'LOCALBOOT_B',
            self.boot_part_info['name'],
            check_cancel=cancelled,
            stderr=True
        )
        if cancelled():
            return True
        if mkfs.returncode != 0:
            return False

        mkfs = run(
            '/usr/sbin/mkfs.ext4',
            '-F', '-F',  # Force overwrite
            '-L', 'LOCALBOOT_SYSTEM',
            self.system_part_info['name'],
            check_cancel=cancelled,
            stderr=True
        )
        if cancelled():
            return True
        if mkfs.returncode != 0:
            return False

        mkfs = run(
            '/usr/sbin/mkfs.ext4',
            '-F', '-F',  # Force overwrite
            '-L', 'LOCALBOOT_HOMES',
            self.homes_part_info['name'],
            check_cancel=cancelled,
            stderr=True
        )
        if cancelled():
            return True
        if mkfs.returncode != 0:
            return False

        run('/usr/bin/udevadm', 'settle')
        self.invalidate_lsblk_info()

        return True


class AppContext:
    def __init__(self, app_config):
        self.config = app_config


    @cached_property
    def lb_system_mount_path(self):
        lb_system_mount_path = WORK_DIR / 'system'
        if lb_system_mount_path.is_mount():
            return lb_system_mount_path

        lb_system_mount_path.mkdir(parents=True, exist_ok=True)
        lb_system_mount_path.chmod(0o700)

        if tcos.system.is_localboot and not tcos.system.is_installer:
            mount = run('/usr/bin/mount',
                        '--bind', '/tcos/link',
                        lb_system_mount_path,
                        stderr=True)
            if mount.returncode != 0:
                raise LocalbootError('Failed to bind mount /tcos/link')
            mount = run('/usr/bin/mount',
                        '-o', 'remount,rw', lb_system_mount_path,
                        stderr=True)
            if mount.returncode != 0:
                raise LocalbootError('Failed to remount /tcos/link')
        else:
            mount = run('/usr/bin/mount',
                        Device.localboot_device.system_part_info['name'],
                        lb_system_mount_path,
                        stderr=True)
            if mount.returncode != 0:
                raise LocalbootError('Failed to mount system partition')

        return lb_system_mount_path


    @cached_property
    def lb_config_dir(self):
        path = self.lb_system_mount_path / 'localboot'
        path.mkdir(parents=True, exist_ok=True)
        return path


    @cached_property
    def lb_boot_mount_path(self):
        lb_boot_mount_path = WORK_DIR / 'boot'
        if lb_boot_mount_path.is_mount():
            return lb_boot_mount_path

        lb_boot_mount_path.mkdir(parents=True, exist_ok=True)
        lb_boot_mount_path.chmod(0o700)

        mount = run('/usr/bin/mount',
                    '-o', 'rw',  # required to avoid random(?) ro mounts
                    Device.localboot_device.boot_part_info['name'],
                    lb_boot_mount_path,
                    stderr=True)
        if mount.returncode != 0:
            raise LocalbootError('Failed to mount boot partition')

        return lb_boot_mount_path


    @property
    def files_source_path(self):
        if tcos.system.is_installer:
            return Path('/tcos/link')
        else:
            return self.nfs_mount_path


    @property
    def boot_files_source_path(self):
        if tcos.system.is_installer:
            inst_boot_mount_path = WORK_DIR / 'boot-src'
            if inst_boot_mount_path.is_mount():
                return inst_boot_mount_path

            inst_boot_mount_path.mkdir(parents=True, exist_ok=True)
            inst_boot_mount_path.chmod(0o700)

            installer_device = Device.installer_device
            if not installer_device:
                logger.error('Installer device not found')
                raise LocalbootError('Installer device not found')
            mount = run('/usr/bin/mount',
                        '-o', 'ro',
                        installer_device.boot_part_info['name'],
                        inst_boot_mount_path,
                        stderr=True)
            if mount.returncode != 0:
                raise LocalbootError('Installer boot partition mount failed')

            return inst_boot_mount_path

        else:
            return self.nfs_mount_path / 'tftp'


    @property
    def nfs_mount_path(self):
        if not tcos.system.is_localboot or tcos.system.is_installer:
            return Path('/tcos/link')

        nfs_mount_path = WORK_DIR / 'nfs'
        if nfs_mount_path.is_mount():
            return nfs_mount_path

        nfs_mount_path.mkdir(parents=True, exist_ok=True)
        nfs_mount_path.chmod(0o700)

        mount = run(
            '/usr/bin/busybox', 'mount',
            '-t', 'nfs',
            '-o', 'nolock,ro,retrans=10,vers=2',
            tcos.system.boot_arguments['nfsroot'],
            nfs_mount_path,
        )
        if mount.returncode != 0:
            raise LocalbootError('Failed to mount NFS root')

        return nfs_mount_path


    @cached_property
    def lb_update_path(self):
        lb_update_path = self.lb_system_mount_path / 'lb_updates'

        if lb_update_path.exists():
            if (lb_update_path / '.apply_updates').exists():
                return lb_update_path
            shutil.rmtree(lb_update_path)  # Remove previous failed update
        lb_update_path.mkdir(parents=True, exist_ok=True)

        sources = (path for path in self.lb_system_mount_path.iterdir()
                        if path.name not in ('lb_updates', 'lost+found'))
        cp = run('/usr/bin/cp', '--archive',
                 *sources, lb_update_path, stderr=True)
        if cp.returncode != 0:
            raise LocalbootError('Failed to copy system for update')

        return lb_update_path


    @cached_property
    def lb_update_config_dir(self):
        path = self.lb_update_path / 'localboot'
        path.mkdir(exist_ok=True)
        return path


    @cached_property
    def localboot_conf(self):
        formatted_mac = f'01-{tcos.system.get_tcos_mac().replace(":", "-")}'
        tftp = run(
            '/usr/bin/busybox', 'tftp', '-g',
            '-r', f'localboot.cfg/{formatted_mac}',
            '-l', '/dev/stdout',
            tcos.system.server,
            stdout=True, stderr=True,
        )
        return tftp.stdout if tftp.returncode == 0 else None

    @cached_property
    def localboot_appends(self):
        if not self.localboot_conf:
            return {}
        return MappingProxyType(dict(
            param.split('=', 1) if '=' in param else (param, 'true')
            for param in self.localboot_conf.split('\n', 1)[0].split()
        ))


class Files:
    dirs = ('custom', 'sfs')

    rsync_cmd  = (
        '/usr/bin/rsync',
        '--recursive', '--links', '--partial', '--times',
        '--delete-before', '--inplace',)


    def __init__(self, app: AppContext):
        self.app = app
        self.bw_limit = re.sub('\D', '', app.config.get('updater.bwLimit', '')
                               ) or '0'


    def install_check(self):
        rsync = run(
            *self.rsync_cmd,
            '--dry-run', '--info=del,progress2',
            *(self.app.files_source_path / dir for dir in self.dirs),
            self.app.lb_system_mount_path,
            stdout=True, stderr=True,
        )
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        return rsync.stdout


    def start_install(self):
        if tcos.system.is_localboot and not tcos.system.is_installer:
            logger.warning('Install requested on localboot system')
            return False

        command = (
            *self.rsync_cmd,
            '--info=progress2',
            *(self.app.files_source_path / dir for dir in self.dirs),
            self.app.lb_system_mount_path,
        )
        logger.debug('Starting command: %s', shlex.join(map(str, command)))
        return subprocess.Popen(
            command,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            encoding='utf-8',
        )


    def update_required(self):
        rsync = run(
            *self.rsync_cmd,
            '--dry-run', '--info=del,progress2',
            *(self.app.files_source_path / dir for dir in self.dirs),
            self.app.lb_system_mount_path,
            stdout=True, stderr=True,
        )
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        return _rsync_will_transfer(rsync.stdout)


    def update(self):
        rsync_cmd = [*self.rsync_cmd]
        rsync = run(
            *rsync_cmd,
            f'--bwlimit={self.bw_limit}',
            *('--info=name,progress2',) if DEBUG else (),
            *(self.app.files_source_path / dir for dir in self.dirs),
            self.app.lb_update_path,
            stdout=DEBUG, stderr=True,
        )
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        return True


class BootloaderConf:
    def __init__(self, ctx: AppContext):
        self.app = ctx


    @cached_property
    def exlinux_conf(self):
        appends = self.app.localboot_appends.copy()

        if self.app.config.get('IPsettings.useDHCP') == 'no':
            appends['ignore_dhcp'] = 'true'

        if tcos.system.is_installer:
            install_stick = False
        else:
            install_stick = self.app.config.get('flasher.installer') == 'yes'

        if install_stick:
            appends['localboot_installer'] = tcos.system.tcos_mac
        else:
            appends.pop('localboot_installer', None)

        return cleandoc("""
            TIMEOUT 15
            PROMPT 1
            DEFAULT openthinclient
            SAY openthinclient localboot
            SAY Press TAB for interactive choice.
            LABEL openthinclient
            KERNEL /{kernel}
            APPEND {append} localboot=true LB_UUID={LB_UUID}
            LABEL debug
            KERNEL /{kernel}
            APPEND {append} localboot=true LB_UUID={LB_UUID} debug=true
            """).format(
                kernel=appends.get('tcoskern', 'vmlinuz_64'),
                append=' '.join(map('='.join, appends.items())),
                LB_UUID=Device.localboot_device.system_part_info['uuid'],
            )


    def install(self):
        try:
            extlinux_conf_path = self.app.lb_boot_mount_path / 'extlinux.conf'
            extlinux_conf_path.write_text(self.exlinux_conf)

            efi_path = self.app.lb_boot_mount_path / 'EFI/BOOT'
            efi_path.mkdir(parents=True, exist_ok=True)
            shutil.copy(extlinux_conf_path, efi_path / 'extlinux.conf')
        except:
            logger.exception('Failed to install bootloader configuration')
            return False

        return True


    def update_required(self):
        extlinux_conf_path = self.app.lb_boot_mount_path / 'extlinux.conf'
        if not extlinux_conf_path.exists():
            return True
        return extlinux_conf_path.read_text() != self.exlinux_conf


    update = install


class Bootloader:

    def __init__(self, app: AppContext):
        self.app = app


    @cached_property
    def kernel_files(self):
        appends = self.app.localboot_appends

        kernel = appends.get('tcoskern', '').lstrip('/')
        if not kernel:
            logger.error('Kernel not found in localboot configuration')
            kernel = 'vmlinuz_64'

        initrds = (file.lstrip('/')
                   for file in appends.get('initrd', '').split(','))
        if not initrds:
            logger.error('Initrd not found in localboot configuration')
            return ()

        return (kernel, *initrds)


    rsync_cmd = (
        '/usr/bin/rsync',
        '--recursive', '--links', '--partial', '--times',
        '--modify-window=1',  # Workaround for FAT32 timestamp resolution
        '--delete-before', '--inplace',
    )


    def install(self, cancelled=None, update=False):
        if cancelled is None:
            cancelled = lambda: False
        logger.debug('Installing legacy bootloader')
        extlinux = run(
            '/opt/localboot/tcos/bin/extlinux',
            '--install' if not update else '--update',
            self.app.lb_boot_mount_path,
            check_cancel=cancelled,
            stderr=True,
        )
        if cancelled():
            return True
        if extlinux.returncode != 0:
            return False

        logger.debug('Installing EFI bootloader')
        efi_path = self.app.lb_boot_mount_path / 'EFI/BOOT'
        efi_path.mkdir(parents=True, exist_ok=True)
        rsync = run(
            *self.rsync_cmd,
            '/opt/localboot/tcos/bin/efi/',
            efi_path,
            check_cancel=cancelled,
            stdout=DEBUG, stderr=True,
        )
        if cancelled():
            return True
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        logger.debug('Copying kernel and initrd')
        if not self.kernel_files:
            return False
        rsync = run(
            *self.rsync_cmd,
            *(self.app.boot_files_source_path / file
              for file in self.kernel_files),
            self.app.lb_boot_mount_path,
            check_cancel=cancelled,
            stdout=DEBUG, stderr=True,
        )
        if cancelled():
            return True
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        return True


    def update_required(self):
        if not self.kernel_files:
            return False

        rsync = run(
            *self.rsync_cmd,
            '--dry-run', '--info=del,progress2',
            *(self.app.boot_files_source_path / file
              for file in self.kernel_files),
            self.app.lb_boot_mount_path,
            stdout=True, stderr=True,
        )
        logger.debug(rsync.stdout)
        if rsync.returncode != 0:
            return False

        return _rsync_will_transfer(rsync.stdout)


    def update(self):
        return self.install(update=True)


class FixedIPScript:
    FILE_NAME = 'tcos-fixedIP.sh'

    SCRIPT_TMPL = cleandoc("""
        #!/bin/bash
        #set -x
        IF=$1
        [ -z "$IF" ] && IF=$TCOS_NIC
        if [ -z "$IF" ] || [ ! -d /sys/class/net/$IF/ ]; then
            if [ ! -d /sys/class/net/eth0/ ] && [ -d /sys/class/net/wlan0/ ]; then
                IF="wlan0"
            else
                IF="eth0"
            fi
        fi

        ip link set $IF up

        ip address flush dev $IF 2> /dev/null
        ip address add {IPandMask} dev $IF

        ip route del default 2> /dev/null
        ip route add {defaultGW} dev $IF
        ip route add default via {defaultGW}

        echo "domain {searchDomain}" > /etc/resolv.conf
        echo "nameserver {nameserver}" >> /etc/resolv.conf

        hostnamectl set-hostname "{hostname}"
        echo "127.0.0.1   {hostname}   localhost" > /etc/hosts
    """)


    def __init__(self, app: AppContext):
        self.app = app

    @cached_property
    def script_path(self):
        return self.app.lb_config_dir / self.FILE_NAME

    @cached_property
    def script_update_path(self):
        return self.app.lb_update_config_dir / self.FILE_NAME


    @cached_property
    def script(self):
        get = self.app.config.get
        variables = {
            'IPandMask'    : get('IPsettings.fixedIP.IPandMask'),
            'defaultGW'    : get('IPsettings.fixedIP.defaultGW'),
            'nameserver'   : get('IPsettings.fixedIP.nameserver'),
            'hostname'     : get('IPsettings.fixedIP.hostname'),
            'searchDomain' : get('IPsettings.fixedIP.searchDomain'),
        }
        if not all(variables.values()):
            return ''

        return self.SCRIPT_TMPL.format(**variables)


    def _write_script(self, path):
        if self.script:
            path.write_text(self.script)
            path.chmod(0o755)
        else:
            path.unlink(missing_ok=True)


    def install(self):
        self._write_script(self.script_path)


    def update_required(self):
        if self.script_path.exists():
            return self.script_path.read_text() != self.script
        return bool(self.script)


    def update(self):
        try:
            self._write_script(self.script_update_path)
        except:
            logger.exception('Failed to update fixed IP script')
            return False
        return True


class WPASupplicantConf:
    FILE_NAME = 'wpa_supplicant.conf'

    CONF_TMPL = cleandoc("""
        ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
        eapol_version=1
        ap_scan=1
        fast_reauth=1
    """)

    FREQ_LIST_2_4 = (
        '2412 2417 2422 2427 2432 2437 2442 2447 2452 2457 2462 2467 2472 2484'
    )
    FREQ_LIST_5 = (
        '5035 5040 5045 5055 5060 5080 5160 5170 5180 5190 5200 5210 5220'
        ' 5230 5240 5250 5260 5270 5280 5290 5300 5310 5320 5340 5480 5500'
        ' 5510 5520 5530 5540 5550 5560 5570 5580 5590 5600 5610 5620 5630'
        ' 5640 5660 5670 5680 5690 5700 5710 5720 5745 5755 5765 5775 5785'
        ' 5795 5805 5815 5825 5835 5845 5855 5865 5875 5885 5900 5910 5920'
        ' 5935 5940 5945 5960 5980'
    )


    def __init__(self, app: AppContext):
        self.app = app


    @cached_property
    def conf_path(self):
        return self.app.lb_config_dir / self.FILE_NAME

    @cached_property
    def conf_update_path(self):
        return self.app.lb_update_config_dir / self.FILE_NAME

    @cached_property
    def conf(self):
        network_blocks = []
        for n in (1, 2, 3):
            if lines := self._network_block_lines(n):
                network_blocks.extend(lines)

        if not network_blocks:
            return ''

        return '\n'.join((self.CONF_TMPL, *network_blocks))


    def _network_block_lines(self, n):
        get = lambda opt: self.app.config.get(
                            f'IPsettings.wifiConnection{n}.{opt}'
                          ) or ''

        if not (ssid := get('ssid').strip()):
            return ()

        if password := get('password'):
            wpa_passphrase = run(
                '/usr/bin/wpa_passphrase', ssid,
                input=password,
                stdout=True, stderr=True
            )
            block = wpa_passphrase.stdout.rstrip('}\n').splitlines()
            if not block:
                return ''
        else:
            block = [
                'network={',
                '\tssid="{}"'.format(ssid),
                '\tkey_mgmt=NONE',
            ]

        band = get('band')
        freqs = None
        if band == '2.4':
            freqs = self.FREQ_LIST_2_4
        elif band == '5':
            freqs = self.FREQ_LIST_5
        if freqs:
            block.append(f'\tfreq_list={freqs}')

        if get('hidden') == 'yes':
            block.append('\tscan_ssid=1')

        bg_threshold = get('bgscan.threshold').strip()
        bg_short     = get('bgscan.short_interval')
        bg_long      = get('bgscan.long_interval')
        if bg_threshold and bg_short and bg_long:
            block.append(
                f'\tbgscan="simple:{bg_short}:{bg_threshold}:{bg_long}"'
            )

        for i in range(1, 3):
            additional_option = get(f'additional_option{i}').strip()
            if additional_option:
                block.append(f'\t{additional_option}')

        block.extend((
            '}',
            ''
        ))
        return block


    def _write_conf(self, path):
        if self.conf:
            path.write_text(self.conf)
            path.chmod(0o600)
        else:
            path.unlink(missing_ok=True)


    def install(self):
        self._write_conf(self.conf_path)


    def update_required(self):
        if self.conf_path.exists():
            return self.conf_path.read_text() != self.conf
        return bool(self.conf)


    def update(self):
        try:
            self._write_conf(self.conf_update_path)
        except:
            logger.exception('Failed to update wpa_supplicant.conf')
            return False
        return True
